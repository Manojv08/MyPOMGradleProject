package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {
	
	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC002_EditLead";
		testCaseDescription ="Edit a lead";
		category = "Unit testing";
		author= "Manoj";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public  void editLead(String FName, String updateCname)  throws InterruptedException {
		
		new MyHomePage()
		.clickLeads()
		.clickFindLead()
		.typeFirstName(FName)
		.clickFindLeads()
		.clickResultingElement()
		.clickEditButton()
		.typeCompanyName(updateCname)
		.clickUpdateButton()
		.verifyCoName();
		
	}

}