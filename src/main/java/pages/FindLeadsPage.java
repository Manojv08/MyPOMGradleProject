package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{
	
	String captureLeadId;
	
	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@CacheLookup
	@FindBy(xpath = "(//input[@name='firstName'])[3]")
	WebElement eleFirstName;
	
	@CacheLookup
	@FindBy(xpath = "//button[text()='Find Leads']")
	WebElement eleFindLeads;
	
	@CacheLookup
	@FindBy(xpath = "//span[text()='Email']")
	WebElement eleEmailTab;	
	
	@CacheLookup
	@FindBy(name = "emailAddress")
	WebElement eleEmailAddr;	
	
	@CacheLookup
	@FindBy(name = "id")
	WebElement eleLeadId;
	
	public FindLeadsPage typeFirstName	(String data) {
//		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, data);
		return this;
	}
	
	public FindLeadsPage clickEmailTab() {
		click(eleEmailTab);
		return this;
		
	}
	
	public FindLeadsPage typeEmailAddress(String data) {
		type(eleEmailAddr, data);
		return this;
	}
	
	public FindLeadsPage typeCapturedLeadId() {
		type(eleLeadId, captureLeadId);
		return this;
	}
		
	public FindLeadsPage clickFindLeads() {
//		WebElement eleFindLeads= locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		return this; 
	}
	
	public ViewLeadPage clickResultingElement() throws InterruptedException {
		Thread.sleep(3000);
		WebElement eleResult = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		captureLeadId = getText(eleResult);
		click(eleResult);
		return new ViewLeadPage();
	}
	
	public void verifyErrorMsg(String ErrMsg) throws InterruptedException {
		Thread.sleep(3000);
		WebElement eleErrMsg = locateElement("class", "x-paging-info");
		verifyExactText(eleErrMsg, ErrMsg);
	}
	
}