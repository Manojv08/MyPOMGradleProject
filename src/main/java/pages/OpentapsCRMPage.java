package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class OpentapsCRMPage extends ProjectMethods{

	public OpentapsCRMPage typeCompanyName(String data) {
		WebElement eleCoName = locateElement("id", "updateLeadForm_companyName");
		type(eleCoName, data);
		return this;
	}

	public ViewLeadPage clickUpdateButton() {
		WebElement eleUpdateBtn= locateElement("class", "smallSubmit");
		click(eleUpdateBtn);
		return new ViewLeadPage(); 
	}

}