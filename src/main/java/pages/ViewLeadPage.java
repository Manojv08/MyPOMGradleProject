package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{
	
	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@CacheLookup
	@FindBy(linkText = "Delete")
	WebElement eleDeleteBtn;

	public OpentapsCRMPage clickEditButton() {
		WebElement eleEditBtn = locateElement("linktext", "Edit");
		click(eleEditBtn);
		return new OpentapsCRMPage();
	}
	
	public void verifyCoName() {
		WebElement eleCoName = locateElement("id", "viewLead_companyName_sp");
		verifyPartialText(eleCoName, "CTS");
	}
	
	public MyLeadsPage clickDeleteButton() {
		click(eleDeleteBtn);
		return new MyLeadsPage();
	}
	
	public DuplicateLeadPage clickDuplicateButton() {
		WebElement eleDupBtn = locateElement("linktext", "Duplicate Lead");
		click(eleDupBtn);
		return new DuplicateLeadPage();
	}
	
}