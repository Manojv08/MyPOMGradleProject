package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{
	
	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@CacheLookup
	@FindBy(id = "createLeadForm_companyName")
	WebElement eleCompanyName;
	
	@CacheLookup
	@FindBy(id = "createLeadForm_firstName")
	WebElement eleFirstName;
	
	@CacheLookup
	@FindBy(id = "createLeadForm_lastName")
	WebElement eleLastName;
	
	@CacheLookup
	@FindBy(className = "smallSubmit")
	WebElement eleCreateLead;
	

	public MergeLeadsPage typeCompanyName(String data) {
//		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, data);
		return this;
	}
	
	public MergeLeadsPage typeFirstName	(String data) {
//		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, data);
		return this;
	}
	public MergeLeadsPage typeLastName	(String data) {
//		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, data);
		return this;
	}
	
	public MergeLeadsPage clickCreateLead() {
//		WebElement eleCreateLead= locateElement("class", "smallSubmit");
		click(eleCreateLead);
		return this; 
	}
	
}









