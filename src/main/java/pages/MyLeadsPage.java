package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@CacheLookup
	@FindBy(linkText = "Merge Leads")
	WebElement eleMergeLd;

	public CreateLeadPage clickCreateLead() {
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
	}

	public FindLeadsPage clickFindLead() {
		WebElement eleFindLead = locateElement("linktext", "Find Leads");
		click(eleFindLead);
		return new FindLeadsPage();
	}

	public MergeLeadsPage clickMergeLead() {
		click(eleMergeLd);
		return new MergeLeadsPage();
	}

}









